package com.example.googlebookapiapp.utils

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager


/**
 * Class that provides utility methods
 *
 * @author Zoran
 * @since 10.4.2019.
 */
class Utils {

    /**
     * Static methods
     */
    companion object {
        /**
         * Hide soft keyboard
         */
        fun hideKeyboard(activity: Activity) {
            // fetch currently focused view
            hideKeyboard(activity, activity.currentFocus)
        }

        /**
         * Hide soft keyboard
         */
        fun hideKeyboard(context: Context, view: View?) {
            val imm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view?.windowToken, 0)
        }
    }
}