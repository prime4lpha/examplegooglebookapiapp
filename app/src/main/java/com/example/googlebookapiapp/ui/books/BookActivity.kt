package com.example.googlebookapiapp.ui.books

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.example.googlebookapiapp.R
import com.example.googlebookapiapp.data.Book
import com.example.googlebookapiapp.databinding.ActivityBookListBinding
import com.example.googlebookapiapp.di.component.DaggerActivityComponent
import com.example.googlebookapiapp.di.module.ActivityModule
import com.example.googlebookapiapp.ui.bookDetails.BookDetailActivity
import com.example.googlebookapiapp.ui.bookDetails.BookDetailFragment
import com.example.googlebookapiapp.utils.Utils
import kotlinx.android.synthetic.main.activity_book_list.*
import kotlinx.android.synthetic.main.book_list.*
import javax.inject.Inject

/**
 * Activity that displays list of books
 *
 * @author Zoran
 * @since 10.4.2019.
 */
class BookActivity: AppCompatActivity(), BookListContract.View, SearchView.OnQueryTextListener {

    companion object {
        const val TAG = "BookActivity"
        const val ARG_SEARCH_QUERY = "$TAG.ArgSearchQuery"
    }

    /** Inject presenter dependency */
    @Inject
    lateinit var presenter: BookListContract.Presenter

    /**
     * DataBinding instance
     */
    private lateinit var binding: ActivityBookListBinding

    /**
     * The adapter for the list of posts
     */
    private val adapter = BookAdapter(this)

    /**
     * Whether or not the activity is in two-pane mode
     */
    private var twoPane: Boolean = false

    /**
     * Variable that holds search text
     */
    private var searchQuery: String? = null
    private var isConfigChange: Boolean = false

    /* ---------------------------------------------------------------------------------------------------------------*/
    /* ----------------------------------------------- Activity methods ----------------------------------------------*/
    /* ---------------------------------------------------------------------------------------------------------------*/

    /**
     * Callback notifying that view is creating
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_book_list)
        binding.adapter = adapter
        binding.layoutManager = LinearLayoutManager(this)
        binding.dividerItemDecoration = DividerItemDecoration(this, DividerItemDecoration.HORIZONTAL)

        injectDependency()

        // attach view to presenter
        presenter.attach(this)

        if (detailContent != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            twoPane = true
        }

        setSupportActionBar(toolbar)

        // initialize item click listener
        adapter.onItemClick = {
            book -> run {
                Log.d(TAG, "onItemClick()")
                navigateToBookDetails(book)
            }
        }

        // initially hide loading indicator
        hideLoadingIndicator()

        // notify presenter that view is created
        presenter.onViewCreated()

        // check if we've undergone configuration change so that we can reload the data
        if (savedInstanceState != null && savedInstanceState.containsKey(ARG_SEARCH_QUERY)) {
            // load search query
            searchQuery = savedInstanceState.getString(ARG_SEARCH_QUERY)
            isConfigChange = true
        }
    }

    /**
     * Callback notifying that view is destroyed
     */
    override fun onDestroy() {
        super.onDestroy()

        // hide soft keyboard
        Utils.hideKeyboard(this)

        // notify presenter that view is destroyed
        presenter.onViewDestroyed()
    }

    /**
     * Callback notifying that view is undergoing configuration change
     */
    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.putString(ARG_SEARCH_QUERY, searchQuery)
        super.onSaveInstanceState(outState)
    }

    /**
     * Create options menu
     */
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        // Inflate menu
        menuInflater.inflate(R.menu.book_list, menu)

        // initialize search view
        val searchItem = menu?.findItem(R.id.search)
        val searchView = searchItem?.actionView as SearchView
        searchView.queryHint = getString(R.string.book_query_hint)
        searchView.setOnQueryTextListener(this)

        // if we already have search query from config change, set it here
        if (searchQuery != null) {
            // expand action view and set query
            searchItem.expandActionView()
            searchView.postDelayed({ searchView.setQuery(searchQuery, true) }, 100)
        }

        return super.onCreateOptionsMenu(menu)
    }

    /**
     * Callback notifying the view that query was submitted
     */
    override fun onQueryTextSubmit(query: String?): Boolean {
        return true
    }

    /**
     * Callback notifying the the view that search text has changed
     */
    override fun onQueryTextChange(newText: String?): Boolean {
        // On configuration change, regardless of the fact we're setting searchQuery to saved value,
        // first call of this method returns empty string, so we need to make sure our saved value is not overwritten
        if (!isConfigChange) {
            // store search string
            searchQuery = newText
        }
        // reset the flag here
        isConfigChange = false

        // start book search by title or author
        presenter.searchBooks(newText)

        return true
    }

    /* ---------------------------------------------------------------------------------------------------------------*/
    /* --------------------------------------------- Presenter callbacks ---------------------------------------------*/
    /* ---------------------------------------------------------------------------------------------------------------*/

    /**
     * Update the current data set
     *
     * @param books list of [Book] to show
     */
    override fun setData(books: List<Book>?) {
        adapter.setData(books)
    }

    /**
     * Update search result count
     *
     * @param count number of search results
     */
    override fun setSearchCount(count: Int) {
        // Not implemented
    }

    /**
     * Displays an error message in the view
     *
     * @param message message to display in the view
     */
    override fun showErrorMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    /**
     * Displays an error message in the view
     *
     * @param messageResId resource id of the error message to display in the view
     */
    override fun showErrorMessage(messageResId: Int) {
        Toast.makeText(this, messageResId, Toast.LENGTH_SHORT).show()
    }

    /**
     * Display loading indicator in the view
     */
    override fun showLoadingIndicator() {
        binding.progressVisibility = View.VISIBLE
    }

    /**
     * Hide loading indicator in the view
     */
    override fun hideLoadingIndicator() {
        binding.progressVisibility = View.GONE
    }

    /**
     * Display empty search indicator in the view
     */
    override fun showEmptyIndicator() {
        binding.emptyVisibility = View.VISIBLE
    }

    /**
     * Hide empty search indicator in the view
     */
    override fun hideEmptyIndicator() {
        binding.emptyVisibility = View.GONE
    }

    /* ---------------------------------------------------------------------------------------------------------------*/
    /* ------------------------------------------------ Other methods ------------------------------------------------*/
    /* ---------------------------------------------------------------------------------------------------------------*/

    /**
     * Inject required dependencies
     */
    private fun injectDependency() {
        DaggerActivityComponent.builder()
            .activityModule(ActivityModule(this))
            .build()
            .inject(this)
    }

    /**
     * Navigate to book details
     */
    private fun navigateToBookDetails(book: Book) {
        // if this is a master-detail flow
        if (twoPane) {
            // attach a fragment to this view
            val fragment = BookDetailFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(BookDetailFragment.ARG_BOOK, book)
                }
            }
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.detailContent, fragment, BookDetailFragment.TAG)
                .commit()
        } else {
            // otherwise, start a new view
            val intent = Intent(this, BookDetailActivity::class.java).apply {
                putExtra(BookDetailFragment.ARG_BOOK, book)
            }
            startActivity(intent)
        }
    }
}