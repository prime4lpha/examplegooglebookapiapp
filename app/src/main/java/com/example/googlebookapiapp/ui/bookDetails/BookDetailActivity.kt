package com.example.googlebookapiapp.ui.bookDetails

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.example.googlebookapiapp.R
import com.example.googlebookapiapp.data.Book
import com.example.googlebookapiapp.ui.books.BookActivity
import kotlinx.android.synthetic.main.activity_book_list.*

/**
 * Activity that displays book details
 *
 * @author Zoran
 * @since 10.4.2019.
 */
class BookDetailActivity: AppCompatActivity() {

    /* ---------------------------------------------------------------------------------------------------------------*/
    /* ----------------------------------------------- Activity methods ----------------------------------------------*/
    /* ---------------------------------------------------------------------------------------------------------------*/

    /**
     * Callback notifying that view is creating
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_book_detail)

        // set toolbar as action bar
        setSupportActionBar(toolbar)
        // Show the Up button in the action bar.
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            val fragment = BookDetailFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(
                        BookDetailFragment.ARG_BOOK,
                        intent.getParcelableExtra<Book>(BookDetailFragment.ARG_BOOK)
                    )
                }
            }

            supportFragmentManager.beginTransaction()
                .add(R.id.detailContent, fragment, BookDetailFragment.TAG)
                .commit()
        }
    }

    /**
     * Callback notifying that menu item was selected
     */
    override fun onOptionsItemSelected(item: MenuItem) =
        when (item.itemId) {
            android.R.id.home -> {
                // navigate to logical parent
                navigateUpTo(Intent(this, BookActivity::class.java))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
}