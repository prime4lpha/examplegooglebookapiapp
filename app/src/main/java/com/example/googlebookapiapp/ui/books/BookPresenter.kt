package com.example.googlebookapiapp.ui.books

import android.util.Log
import com.example.googlebookapiapp.R
import com.example.googlebookapiapp.remote.BookApi
import com.example.googlebookapiapp.utils.API_KEY
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

/**
 * Presenter that will handle [BookListContract.View] presentation
 *
 * @property bookApi the API interface implementation
 * @property context the context in which the application is running
 * @property subscription the subscription to the API call
 *
 * @author Zoran
 * @since 10.4.2019.
 */
class BookPresenter: BookListContract.Presenter {

    private val tag: String = "BookPresenter"

    /** Instantiate [BookApi] */
    val bookApi: BookApi = BookApi.create()

    /** Reference to view */
    private lateinit var view: BookListContract.View

    /**
     * Variable to handle data subscription
     */
    private var subscription: Disposable? = null

    /**
     * Callback notifying the view is created
     */
    override fun onViewCreated() {
        super.onViewCreated()
        Log.d(tag, "onViewCreated()")
    }

    /**
     * Callback notifying the view is destroyed
     */
    override fun onViewDestroyed() {
        super.onViewDestroyed()
        Log.d(tag, "onViewDestroyed()")
        // destroy subscription
        subscription?.dispose()
    }
    /**
     * Attach view to this presenter
     *
     * @param view [BookListContract.View] to attach
     */
    override fun attach(view: BookListContract.View) {
        this.view = view
    }

    /**
     * Start searching for books
     *
     * @param searchString text by which the search will be performed
     */
    override fun searchBooks(searchString: String?) {
        Log.d(tag, "searchBooks(): searchString=$searchString")

        // if search string is present, start the search
        // Note: I'm limiting the min search length to 3 so that we avoid large data sets in search results
        if (searchString?.isNotEmpty()!! && searchString.length >= 3) {
            view.showLoadingIndicator()
            view.hideEmptyIndicator()

            subscription = bookApi
                .searchBooks(searchString, API_KEY)
                .debounce(300, TimeUnit.MILLISECONDS)
                .distinctUntilChanged()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnTerminate {
                    view.hideLoadingIndicator()
                }
                .subscribe(
                    { volumes ->
                        run {
                            Log.d(tag, "searchBooks(): returned results " + volumes.items.count())
                            view.setData(volumes.items)
                            view.setSearchCount(volumes.items.count())

                            // check if result set is empty or not
                            if (volumes.items.isNotEmpty()) {
                                view.hideEmptyIndicator()
                            } else {
                                view.showEmptyIndicator()
                            }
                        }
                    },
                    { error ->
                        run {
                            Log.e(tag, "searchBooks(): $error")
                            view.showErrorMessage(R.string.book_error_search)
                        }
                    }
                )
        }
        // otherwise, clear search results
        else {
            view.hideLoadingIndicator()
            view.showEmptyIndicator()
            view.setData(null)
        }
    }
}