package com.example.googlebookapiapp.ui.bookDetails

import android.util.Log
import com.example.googlebookapiapp.data.Book

/**
 * Presenter that will handle [BookDetailView] presentation
 *
 * @author Zoran
 * @since 10.4.2019.
 */
class BookDetailPresenter: BookDetailsContract.Presenter {

    private val tag: String = "BookDetailPresenter"

    /** Reference to view */
    private lateinit var view: BookDetailsContract.View

    /**
     * Callback notifying the view is created
     */
    override fun onViewCreated() {
        super.onViewCreated()
        Log.d(tag, "onViewCreated()")
    }

    /**
     * Callback notifying the view is destroyed
     */
    override fun onViewDestroyed() {
        super.onViewDestroyed()
        Log.d(tag, "onViewDestroyed()")
    }

    /**
     * Attach view to this presenter
     *
     * @param view [BookDetailsContract.View] to attach
     */
    override fun attach(view: BookDetailsContract.View) {
        this.view = view
    }

    /**
     * Retrieve the book
     *
     * @param book [Book]
     */
    override fun loadBook(book: Book?) {
        Log.d(tag, "loadBook(): $book")

        view.setData(book)
    }
}