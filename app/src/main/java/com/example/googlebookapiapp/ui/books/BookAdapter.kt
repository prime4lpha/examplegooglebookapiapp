package com.example.googlebookapiapp.ui.books

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.googlebookapiapp.R
import com.example.googlebookapiapp.data.Book
import com.example.googlebookapiapp.databinding.ListItemBookBinding

/**
 * Adapter for the list of the posts
 * @property context [Context] in which the application is running
 *
 * @author Zoran
 * @since 10.4.2019.
 */
class BookAdapter(private val context: Context): RecyclerView.Adapter<BookAdapter.ViewHolder>() {

    /** Item click listener */
    var onItemClick: ((Book) -> Unit)? = null

    /** list of books */
    private var books: List<Book>? = listOf()

    /**
     * Update the current data set
     *
     * @param books list of [Book] to show
     */
    fun setData(books: List<Book>?) {
        this.books = books
        notifyDataSetChanged()
    }

    /**
     * Retrieve current item count
     */
    override fun getItemCount(): Int {
        return if (books != null)
            books!!.size
        else
            0
    }

    /**
     * Create view holder
     *
     * @return [ViewHolder]
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookAdapter.ViewHolder {
        val layoutInflater = LayoutInflater.from(context)
        val binding: ListItemBookBinding = DataBindingUtil.inflate(layoutInflater, R.layout.list_item_book, parent, false)
        return ViewHolder(binding)
    }

    /**
     * Bind data to view holder
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(books!![position])
    }

    /**
     * The ViewHolder of the adapter
     *
     * @property binding the DataBinding object for [Book] item
     */
    inner class ViewHolder(private val binding: ListItemBookBinding): RecyclerView.ViewHolder(binding.root) {

        init {
            // initialize the onClickListener for VH root
            itemView.setOnClickListener {
                onItemClick?.invoke(books!![adapterPosition])
            }
        }

        /**
         * Bind [Book] into the view
         */
        fun bind(book: Book) {
            binding.book = book
            binding.executePendingBindings()
        }
    }
}