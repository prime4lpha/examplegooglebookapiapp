package com.example.googlebookapiapp.ui.bookDetails

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.googlebookapiapp.R
import com.example.googlebookapiapp.data.Book
import com.example.googlebookapiapp.databinding.FragmentBookDetailsBinding
import com.example.googlebookapiapp.di.component.DaggerFragmentComponent
import com.example.googlebookapiapp.di.module.FragmentModule
import javax.inject.Inject

/**
 * Fragment that displays book details
 *
 * @author Zoran
 * @since 10.4.2019.
 */
class BookDetailFragment: Fragment(), BookDetailsContract.View {

    companion object {
        const val TAG: String = "BookDetailFragment"

        /** The fragment argument representing the book ID that this fragment represents. */
        const val ARG_BOOK: String = "$TAG.ExtraBook"
    }

    /** Inject presenter dependency */
    @Inject
    lateinit var presenter: BookDetailsContract.Presenter

    /**
     * DataBinding instance
     */
    private lateinit var binding: FragmentBookDetailsBinding

    /* ---------------------------------------------------------------------------------------------------------------*/
    /* ----------------------------------------------- Fragment methods ----------------------------------------------*/
    /* ---------------------------------------------------------------------------------------------------------------*/

    /**
     * Callback notifying that view is creating
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectDependency()
    }

    /**
     * Callback notifying that view is creating
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // inflate the view
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_book_details, container, false)
        return binding.root
    }

    /**
     * Callback notifying that view is created
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // add view reference to the presenter
        presenter.attach(this)

        arguments?.let {
            if (it.containsKey(ARG_BOOK)) {
                // start loading the book into view
                presenter.loadBook(it.getParcelable(ARG_BOOK))
            }
        }
    }

    /* ---------------------------------------------------------------------------------------------------------------*/
    /* --------------------------------------------- Presenter callbacks ---------------------------------------------*/
    /* ---------------------------------------------------------------------------------------------------------------*/

    /**
     * Update the current data set
     *
     * @param book [Book] to show
     */
    override fun setData(book: Book?) {
        binding.book = book
    }

    /**
     * Displays an error message in the view
     *
     * @param message message to display in the view
     */
    override fun showErrorMessage(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }

    /**
     * Displays an error message in the view
     *
     * @param messageResId resource id of the error message to display in the view
     */
    override fun showErrorMessage(messageResId: Int) {
        Toast.makeText(activity, messageResId, Toast.LENGTH_SHORT).show()
    }

    /* ---------------------------------------------------------------------------------------------------------------*/
    /* ------------------------------------------------ Other methods ------------------------------------------------*/
    /* ---------------------------------------------------------------------------------------------------------------*/

    /**
     * Inject required dependencies
     */
    private fun injectDependency() {
        DaggerFragmentComponent.builder()
            .fragmentModule(FragmentModule())
            .build()
            .inject(this)
    }
}