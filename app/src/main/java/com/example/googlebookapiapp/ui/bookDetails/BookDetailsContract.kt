package com.example.googlebookapiapp.ui.bookDetails

import com.example.googlebookapiapp.core.BaseContract
import com.example.googlebookapiapp.data.Book

/**
 * Contract class for book details
 *
 * @author Zoran
 * @since 10.4.2019.
 */
class BookDetailsContract {

    interface View: BaseContract.View {
        /**
         * Update the current data set
         *
         * @param book [Book] to show
         */
        fun setData(book: Book?)
    }

    interface Presenter: BaseContract.Presenter<View> {
        /**
         * Retrieve the book
         *
         * @param book [Book]
         */
        fun loadBook(book: Book?)
    }
}