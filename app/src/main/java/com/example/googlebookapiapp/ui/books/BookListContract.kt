package com.example.googlebookapiapp.ui.books

import com.example.googlebookapiapp.core.BaseContract
import com.example.googlebookapiapp.data.Book

/**
 * Contract class for book list
 *
 * @author Zoran
 * @since 10.4.2019.
 */
class BookListContract {

    interface View: BaseContract.View {
        /**
         * Update the current data set
         *
         * @param books list of [Book] to show
         */
        fun setData(books: List<Book>?)

        /**
         * Update search result count
         *
         * @param count number of search results
         */
        fun setSearchCount(count: Int)

        /**
         * Display loading indicator in the view
         */
        fun showLoadingIndicator()

        /**
         * Hide loading indicator in the view
         */
        fun hideLoadingIndicator()

        /**
         * Display empty search indicator in the view
         */
        fun showEmptyIndicator()

        /**
         * Hide empty search indicator in the view
         */
        fun hideEmptyIndicator()
    }

    interface Presenter: BaseContract.Presenter<View> {

        /**
         * Start searching for books
         *
         * @param searchString text by which the search will be performed
         */
        fun searchBooks(searchString: String?)
    }
}