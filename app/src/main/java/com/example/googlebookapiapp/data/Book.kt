package com.example.googlebookapiapp.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Book data model
 *
 * @property id the unique identifier of the book
 * @property volumeInfo [VolumeInfo] class containing all info about the book
 *
 * @author Zoran
 * @since 10.4.2019.
 */
@Parcelize
data class Book(val id: String,
                val volumeInfo: VolumeInfo): Parcelable