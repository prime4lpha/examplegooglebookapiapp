package com.example.googlebookapiapp.data

/**
 * Volumes data model
 *
 * @property totalItems number of returned results
 * @property items list of [Book] returned in data set
 *
 * @author Zoran
 * @since 10.4.2019.
 */
data class Volumes(
    val totalItems: Int,
    val items: List<Book>
)