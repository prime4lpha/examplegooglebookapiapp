package com.example.googlebookapiapp.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Book image links data model
 *
 * @param smallThumbnail small thumbnail URL
 * @param thumbnail thumbnail URL
 *
 * @author Zoran
 * @since 10.4.2019.
 */
@Parcelize
data class ImageLinks(
    val smallThumbnail: String?,
    val thumbnail: String?
): Parcelable