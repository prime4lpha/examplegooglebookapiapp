package com.example.googlebookapiapp.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Volume Info data model
 *
 * @author Zoran
 * @since 10.4.2019.
 */
@Parcelize
data class VolumeInfo(
    val title: String?,
    val subtitle: String?,
    val authors: List<String>?,
    val publisher: String?,
    val publishedDate: String?,
    val description: String?,
    val pageCount: Int?,
    val language: String?,
    val averageRating: Float?,
    val ratingsCount: Int?,
    val imageLinks: ImageLinks?
): Parcelable