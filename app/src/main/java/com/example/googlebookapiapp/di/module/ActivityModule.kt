package com.example.googlebookapiapp.di.module

import android.app.Activity
import com.example.googlebookapiapp.ui.books.BookListContract
import com.example.googlebookapiapp.ui.books.BookPresenter
import dagger.Module
import dagger.Provides

/**
 * Module that provides all dependencies for Activity
 *
 * @author Zoran
 * @since 10.4.2019.
 */
@Module
class ActivityModule(private var activity: Activity) {

    /**
     * Provides the Activity
     *
     * @return [Activity] to be provided
     */
    @Provides
    fun provideActivity(): Activity {
        return activity
    }

    /**
     * Provides the Book presenter
     *
     * @return [BookListContract.Presenter] to be provided
     */
    @Provides
    fun providePresenter(): BookListContract.Presenter {
        return BookPresenter()
    }
}