package com.example.googlebookapiapp.di.component

import com.example.googlebookapiapp.di.module.FragmentModule
import com.example.googlebookapiapp.ui.bookDetails.BookDetailFragment
import dagger.Component

/**
 * Component for providing dependency on fragment
 *
 * @author Zoran
 * @since 10.4.2019.
 */
@Component(modules = [FragmentModule::class])
interface FragmentComponent {

    /**
     * Injects required dependencies into the specified Fragment.
     *
     * @param bookDetailFragment [BookDetailFragment] in which to inject the dependencies
     */
    fun inject(bookDetailFragment: BookDetailFragment)
}