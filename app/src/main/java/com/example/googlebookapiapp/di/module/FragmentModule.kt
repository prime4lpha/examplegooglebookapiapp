package com.example.googlebookapiapp.di.module

import com.example.googlebookapiapp.ui.bookDetails.BookDetailsContract
import com.example.googlebookapiapp.ui.bookDetails.BookDetailPresenter
import dagger.Module
import dagger.Provides

/**
 * Module for providing dependencies for fragments
 *
 * @author Zoran
 * @since 10.4.2019.
 */
@Module
class FragmentModule {

    @Provides
    fun provideBookDetailsPresenter(): BookDetailsContract.Presenter {
        return BookDetailPresenter()
    }
}