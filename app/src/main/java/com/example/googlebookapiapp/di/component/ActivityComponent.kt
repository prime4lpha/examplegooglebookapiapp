package com.example.googlebookapiapp.di.component

import com.example.googlebookapiapp.di.module.ActivityModule
import com.example.googlebookapiapp.ui.books.BookActivity
import dagger.Component
import javax.inject.Singleton

/**
 * Component for providing dependency on activity
 *
 * @author Zoran
 * @since 10.4.2019.
 */
@Singleton
@Component(modules = [ActivityModule::class])
interface ActivityComponent {

    /**
     * Injects required dependencies into the specified Activity.
     *
     * @param bookActivity [BookActivity] in which to inject the dependencies
     */
    fun inject(bookActivity: BookActivity)
}