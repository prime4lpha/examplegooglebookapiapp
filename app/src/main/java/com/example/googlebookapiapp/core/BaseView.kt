package com.example.googlebookapiapp.core

import android.content.Context

/**
 * Base interface that every view must implement
 *
 * @author Zoran
 * @since 10.4.2019.
 */
interface BaseView {

    /**
     * Get the context in which the application is running
     *
     * @return [Context] im which the application is running
     */
    fun getContext(): Context

    /**
     * Get presenter instance for this view
     *
     * @return [BasePresenter] for this view
     */
//    fun getPresenter(): BasePresenter<BaseView>
}