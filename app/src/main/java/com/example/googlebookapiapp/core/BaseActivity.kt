package com.example.googlebookapiapp.core

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity

/**
 * Base activity that all activities must extend to provide proper presenter instantiation
 *
 * @author Zoran
 * @since 10.4.2019.
 */
abstract class BaseActivity<P: BaseContract.Presenter<BaseView>> : BaseContract.View, AppCompatActivity() {

    protected lateinit var presenter: P

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = createPresenter()
    }

    /**
     * Create an instance of the presenter for the activity
     */
    protected abstract fun createPresenter(): P

    /**
     * Get the context in which the application is running
     *
     * @return [Context] im which the application is running
     */
    fun getContext(): Context {
        return this
    }
}