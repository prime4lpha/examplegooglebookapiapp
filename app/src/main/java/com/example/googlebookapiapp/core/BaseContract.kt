package com.example.googlebookapiapp.core

import android.support.annotation.StringRes

/**
 * Base contract class
 *
 * @author Zoran
 * @since 10.4.2019.
 */
class BaseContract {

    /**
     * Base view
     */
    interface View {
        /**
         * Displays an error message in the view
         *
         * @param message message to display in the view
         */
        fun showErrorMessage(message: String)

        /**
         * Displays an error message in the view
         *
         * @param messageResId resource id of the error message to display in the view
         */
        fun showErrorMessage(@StringRes messageResId: Int)
    }

    /**
     * Base presenter
     */
    interface Presenter<in V> {
        /**
         * Callback notifying the view is created
         */
        fun onViewCreated() {}

        /**
         * Callback notifying the view is destroyed
         */
        fun onViewDestroyed() {}

        /**
         * Attach view to this presenter
         *
         * @param view [BaseContract.View] to attach
         */
        fun attach(view: V)
    }
}