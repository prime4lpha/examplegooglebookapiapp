package com.example.googlebookapiapp.remote

import com.example.googlebookapiapp.data.Volumes
import com.example.googlebookapiapp.utils.BASE_URL
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * The interface which provides methods to access remote repository
 *
 * @author Zoran
 * @since 10.4.2019.
 */
interface BookApi {

    /**
     * Get the list of books from the API
     *
     * @param searchString text by which the search filtering will be performed
     * @param apiKey Google API key needed for request authorization
     */
    @GET("/books/v1/volumes")
    fun searchBooks(@Query("q") searchString: String,
                    @Query("key") apiKey: String)
            : Observable<Volumes>

    companion object Factory {

        /**
         * Provides the BookApi service implementation.
         *
         * @return [BookApi] service implementation.
         */
        fun create(): BookApi {
            // for the purpose of logging errors on requests
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build()

            return retrofit.create(BookApi::class.java)
        }
    }
}